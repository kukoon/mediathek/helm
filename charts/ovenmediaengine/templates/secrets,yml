{{- $nameAPITokenSecret :=  .Values.ovenmediaengine.apitokenSecret.secretName | default (print (include "ovenmediaengine.fullname" .) "-api-token") }}
{{- $nameSignedPolicySecret :=  .Values.ovenmediaengine.signedPolicy.secretName | default (print (include "ovenmediaengine.fullname" .) "-signed-policy") }}
{{- if not .Values.ovenmediaengine.apitokenSecret.exists }}
---
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: "{{ $nameAPITokenSecret }}"
  labels:
    {{- include "ovenmediaengine.labels" . | nindent 4 }}
  annotations:
    "helm.sh/resource-policy": "keep"
data:
  {{- $secretObj := (lookup "v1" "Secret" .Release.Namespace $nameAPITokenSecret) | default dict }}
  {{- $secretData := (get $secretObj "data") | default dict }}
  {{- $token := (get $secretData .Values.ovenmediaengine.apitokenSecret.key) | default (randAlphaNum 32 | b64enc) }}
  # generate 32 chars long random string, base64 encode it and then double-quote the result string.
  {{ .Values.ovenmediaengine.apitokenSecret.key }}: {{ $token | quote }}
{{- end }}
{{- if and
  .Values.ovenmediaengine.signedPolicy.enabled
  (not .Values.ovenmediaengine.signedPolicy.existingSecret)
}}
---
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: "{{ $nameSignedPolicySecret }}"
  labels:
    {{- include "ovenmediaengine.labels" . | nindent 4 }}
  annotations:
    "helm.sh/resource-policy": "keep"
data:
  {{- $secretObj := (lookup "v1" "Secret" .Release.Namespace $nameSignedPolicySecret) | default dict }}
  {{- $secretData := (get $secretObj "data") | default dict }}
  {{- $token := (get $secretData .Values.ovenmediaengine.signedPolicy.key) | default (randAlphaNum 32 | b64enc) }}
  # generate 32 chars long random string, base64 encode it and then double-quote the result string.
  {{ .Values.ovenmediaengine.signedPolicy.key }}: {{ $token | quote }}
{{- end }}